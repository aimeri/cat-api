require('dotenv').config()
const express = require("express")
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')
mongoURI = `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`
let catSchema = new mongoose.Schema({
    Title: String,
    Description: String,
    Price: String,
    Location: String,
    Pictures: [String],
    Link: String,
    Posted: String,
    Emailed: {
        type: Number,
        default: 0
    }
});

let Cat = mongoose.model('cat-scraping', catSchema);

app.use(cors())

app.get("/", (req, res) => {
    if (mongoose.connection.readyState == 0) {
        const config = { useNewUrlParser: true, useFindAndModify: false };
        mongoose.connect(mongoURI, config);
    }
    Cat.find({}, 'Title Description Price Location Pictures Posted Link').lean().exec(function (err, cats) {
        return res.end(JSON.stringify(cats))
    })
})

const PORT = process.env.PORT || 5000;
app.listen(PORT, function () {
    console.log(`App listening on port ${PORT}`);
});
